window.addEventListener("load", () => {
	const app = new Vue({
		el: "#app",
		data: {
			message: "",
			messages: [],
		},
		created: function() {
			// Run the intro when the component is created

			// Add message to queue
			this.addTypingMessage();

			setTimeout(() => {
				this.messages.pop();
				this.addMessage({
					id: Date.now(),
					type: "remote",
					text: "Oh hey, didn't see you there! 👋",
				});
			}, 1500);

			setTimeout(() => {
				this.addTypingMessage();
			}, 2000);

			setTimeout(() => {
				this.messages.pop();
				this.addMessage({
					id: Date.now(),
					type: "remote",
					text: "I'm Fabi's Bot. 🙆‍♂️",
				});
			}, 2750);
		},
		methods: {

			// Add a "user is typing" message.
			addTypingMessage: function() {
				this.messages.push({
					id: Date.now(),
					type: "remote",
					loading: true,
				});

				// Scroll down
				this.$emit("scroll-down");
			},

			// Add a message
			addMessage: function(message) {
				this.messages.push(message);

				// Scroll down
				this.$emit("scroll-down");
			},

			onSubmit: function() {
				console.log("submit", app.message);

				const message = app.message;

				// Add message to queue
				this.addMessage({
					id: Date.now(),
					type: "local",
					text: app.message,
				});

				// Push "is typing"
				setTimeout(() => {
					this.addTypingMessage();

					// Send the message to the server.
					console.log(message);
					fetch(`/api/query/${message}`)
						.then((response) => response.json())
						.then((response) => {
							console.log(response);

							// Remove "is-typing"
							app.messages.pop();

							// Push actual response
						   this.addMessage(response);
						});
				}, 1000);


				// Clear the message
				Vue.set(app, "message", "");
			}
		},
	});


});
