// Job information intent
// ---
// Description
// ------------------------------------------------------------------------- /
const handleIntent = (request) => {

	const projects = [
		{
			id: "1",
			name: "NewProject",
			url: "http://www.google.be",
		},
		{
			id: "2",
			name: "NewProject2",
			url: "http://www.google.be",
		},
		{
			id: "3",
			name: "NewProject3",
			url: "http://www.google.be",
		},
	];

	const displayText = `I've done quite a few projects. The one I'm most proud of is ${projects[0].name}. You can view this project at <a href="${projects[0].url}">${projects[0].url}</a>. <img src="img/doge.jpg" />`;
	const speech = `I've done quite a few projects. The one I'm most proud of is ${projects[0].name}. You can view this project at ${projects[0].url}.`;


	return {
		"speech": speech,
		"displayText": displayText,
		// "data": { ...},
		// "contextOut": [...],
		// "source": "DuckDuckGo",
	};
};

module.exports = {
	handleIntent: handleIntent,
};
