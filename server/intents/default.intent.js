const handleIntent = (request) => {
	return {
		"speech": request.fulfillment.speech,
		"displayText": request.fulfillment.speech,
	};
};

module.exports = {
	handleIntent: handleIntent,
};
