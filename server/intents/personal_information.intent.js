// Personal information intent
// ---
// Description
// ------------------------------------------------------------------------- /
const handleIntent = (request) => {

	console.log(request.parameters.family);

	const familyTree = {
		brothers: [
			{
				name: "Matthias",
				age: "32",
			}
		],
		sisters: [],
	};

	let displayText = "";
	let speech = "";

	switch (request.parameters.family) {
		case "brother":
			displayText = `I've got ${familyTree.brothers.length} brother. 🤘`;
			speech = `I've got ${familyTree.brothers.length} brother.`;
			break;

		default:
	}


	return {
		"speech": speech,
		"displayText": displayText,
		// "data": { ...},
		// "contextOut": [...],
		// "source": "DuckDuckGo",
	};
};

module.exports = {
	handleIntent: handleIntent,
};
