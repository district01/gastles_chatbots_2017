// IntentController
// ------------------------------------------------------------------------- /

const jobInformationIntent = require("./job_information.intent");
const personalInformationIntent = require("./personal_information.intent");
const defaultIntent = require("./default.intent");

const handleIntent = (request) => {

	switch (request.metadata.intentName) {
		case "job_information":
			return jobInformationIntent.handleIntent(request);
		case "personal_information":
			return personalInformationIntent.handleIntent(request);

		default:
			return defaultIntent.handleIntent(request);
	}
};

module.exports = {
	handleIntent: handleIntent,
};
