# Seminar Chatbots 2017

## The slides

https://drive.google.com/file/d/1pXh8tBqFmiFV8ar4RoiGMKb-IUfHlmsk/view?usp=sharing


## Dependencies

* Node
* [Ngrok](https://ngrok.com/download)

## Code usage

**Install the dependencies**

- `npm i`
- `cd ./app && npm i`

**Start the application**

- `npm start`

**Open the application in the browser**

- `http://localhost:4000/`


## Project structure

* **Server**: NodeJS server.
* **App**: VueJS application.


## Project Steps
* **steps/step1**: Setup Routing.
* **steps/step2**: Call the DialogFlow API.
* **steps/step3**: Handle intents.
* **steps/step4**: Add user delight.


## DialogFlow
---

### DialogFlow Intents
* **personal_information**: Ask about personal information.
* **job_information**: Ask about job-related things.

### DialogFlow Entities
* **family**: People and pets.
* **jobs**:
* **opinions**: Adjectives that indicate "opinions"
* **sports**: Hobbies and Sports